/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application;

import application.elements.Inimigo;
import application.elements.Personagem;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import engine.Game;
import engine.GameEngine;

/**
 *
 * @author wsiqueir
 */
public class TowerDefense extends Application {

    @Override
    public void start(Stage s) {
        // Esse é o tamanho da tela do nosso jogo, a área de desenho
        final int largura = 800, altura = 550;
        
        Game towerDef = new Game(largura, altura) {
                
            // A velocidade que o inimigo vai se mover
            float velocidadeX = 0.4f, velocidadeY = 0.4f;
            
            // a posição X e Y do inimigo
            float x = largura, y = altura/2;
            
            Personagem inimigo = new Inimigo(x, y);
            
            @Override
            public void update() {
                // o inimigo terá sempre a sua posição atualizada
                x += velocidadeX;
                inimigo.setPosicaoX(x);
                
                //y += velocidadeY;
                // Verificamos se a posição do inimigo está saindo da tela
                if (inimigo.getPosicaoX() > largura || inimigo.getPosicaoX() < 0) {
                    velocidadeX *= -1;
                }
               /* if (y > altura || y < 0) {
                    velocidadeY *= -1;
                }*/
            }

            @Override
            public void display() {
                //gc = gameContext, pra poder desenhar
                
                // primeiro desenhos o fundo com a cor cinza claro
                gc.setFill(Color.LIGHTGRAY);
                gc.fillRect(0, 0, largura, altura);
                
                
                // inimigo vermelho vermelha
                gc.setFill(Color.DARKRED);
                // enfim, desenhamos o inimigo com a posição atualizada e o tamanho de 10
                gc.fillOval(inimigo.getPosicaoX(), inimigo.getPosicaoY(), 20, 20);
                
                
                //torre
                gc.setFill(Color.DARKBLUE);
                gc.fillRect(5, 5, 30, 30);
                
                
                //armadilha
                gc.setFill(Color.DARKKHAKI);
                gc.fillRect(500, 500, 30, 30);
                
                
                //armadilha
                gc.setFill(Color.DARKKHAKI);
                gc.fillRect(350, 300, 30, 30);
                
                
            }
        };
        
        // aqui a nossa engine em sí, sem ela o jogo não tem vida. 
        //Colocamos 100 quadros por segundos, 
        //o que significa que a tela vai se desenhar 100 vezes por segundo
        GameEngine engine = new GameEngine(100, towerDef);
        
        // Agora vamos fazer as coisas do JavaFX para ter a nossa aplicação aparecendo quando rodarmos o programa
        s.setScene(new Scene(new StackPane(towerDef.getCanvas())));
        s.show();

        // taca lhe pau!
        engine.start();
        
        
    }

}

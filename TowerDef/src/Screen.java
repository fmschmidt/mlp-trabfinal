import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.image.CropImageFilter;
import java.awt.image.FilteredImageSource;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

/*
 * Nossa screen que vamos poder desenhar nela.
 * Podemos adicionar mais componentes no JPanel e tals.
 * Ele tem o proprio tamanho. T� na area entre as bordas.
 * */
public class Screen extends JPanel implements Runnable{

	private Thread gameLoop = new Thread(this);
	private static boolean isFirst = true;
	public static boolean isDebug = true;

	public static int myWidth, myHeight;
	public static int health = 100;

	public static Image[] tileset_ground = new Image[100];
	public static Image[] tileset_air = new Image[100];
	public static Image[] tileset_tower = new Image[100];
	public static Image[] tileset_enemy = new Image[100];
	public static Image[] tileset_trap = new Image[100];

	public static Room room;
	private static Save save;

	public static Enemy[] enemies = new Enemy[100];

	public static Point mouse = new Point (0,0);

	public static Options options;
	//tempo de nascimento do inimigo
	private int spawnTime = 2400, spawnFrame = 0;


	//construtor
	public Screen(Frame frame){
		//setBackground(Color.PINK);
		frame.addMouseListener(new KeyHandler());
		frame.addMouseMotionListener(new KeyHandler());

		gameLoop.start();
	}

	/*
	 *
	 * */
	private void define(){
		room = new Room();
		save = new Save();
		options = new Options();

		//desenhar os blocos
		for(int i = 0; i < tileset_ground.length; i++){
			tileset_ground[i] = new ImageIcon("resources/groundGrass.png").getImage();
			tileset_ground[i] = createImage(new FilteredImageSource(tileset_ground[i].getSource(), new CropImageFilter(0, 26*i, 26, 26)));
		}

		for(int i = 0; i < tileset_air.length; i++){
			tileset_air[i] = new ImageIcon("resources/groundAir.png").getImage();
			tileset_air[i] = createImage(new FilteredImageSource(tileset_air[i].getSource(), new CropImageFilter(0, 26*i, 26, 26)));
		}

		tileset_tower[0] = new ImageIcon("resources/tower.png").getImage();
		tileset_enemy[0] = new ImageIcon("resources/enemy.png").getImage();
		tileset_trap[0] = new ImageIcon("resources/trap.png").getImage();

		save.loadSave(new File("save/level1.felipe"));

		for(int i=0; i<enemies.length; i++){
			enemies[i] = new Enemy(); //definindo todos os inimigos

		}

	}


	private void enemySpawner(){
		if(spawnFrame >= spawnTime){ //if it reaches spawnTime
			for(int i=0; i< enemies.length; i++){
				if(!enemies[i].inGame){
					enemies[i].spawnEnemy(Value.enemyGreeny);
					break;
				}
			}

			spawnFrame = 0;
		}else {
			spawnFrame += 1;
		}
	}

	/*
	 * Classe dentro de JPanel (implementando a classe que ja existe)
	 * */
	public void paintComponent(Graphics g){

		if(isFirst){
			myWidth = getWidth();
			myHeight = getHeight();

			define();
			isFirst = false;
		}

		//clean the screen after doing a lot of things
		//agora pinta a tela de preto
		g.setColor(new Color (50, 50, 50));
		g.fillRect(0, 0, getWidth(), getHeight());

		//pinta os botoes abaixo
		g.setColor(new Color (0,0,0));

		room.draw(g);	//drawing the room.

		for(int i=0; i < enemies.length; i++){
			if(enemies[i].inGame){
				enemies[i].draw(g);
			}
		}

		options.draw(g);	//drawing the option buttons

		//show "game over"
		if(health < 1){
			g.setColor(new Color (240, 20, 20));
			g.fillRect(0, 0, myWidth, myHeight);
			g.setColor(new Color(255, 255, 255));
			g.setFont(new Font("Courier New", Font.BOLD, 20));
			g.drawString("Game Over!", 50, 50);
		}

	}

	/*
	 * Game loop
	 * ex: speed, characters, how they walk around, etc..
	 *
	 * */
	public void run(){
		while(true){
			if(!isFirst && health > 0){
				room.physic();
				enemySpawner(); //gerador do inimigo
				for(int i=0; i < enemies.length; i++){
					if(enemies[i].inGame){
						enemies[i].physic();	//physic class from the enemy.
					}
				}
			}

			repaint();

			try{
				Thread.sleep(1);
			}catch(Exception e){ }
		}
	}

}

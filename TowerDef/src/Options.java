import java.awt.*;

/*
 * cria opcoes de criar torre, etc
 * */
public class Options {

	private static int optionsWidth = 8;
	private Rectangle[] button = new Rectangle[optionsWidth];
	private static int buttonSize = 52;	//vai ser 26 de novo
	private static int cellSpace = 2; //space between cells (separacao entre botoes)
	private static int awayFromRoom = 29;	//para posicionar os botoes abaixo do mapa

	private static int itemIn = 4;		//quao grande a borda do item vai ser

//	public static int[] buttonID = {Value.airTowerLaser, Value.airAir, Value.airAir, Value.airAir, Value.airAir, Value.airAir, Value.airAir, Value.airTrashCan};
	private static int[] buttonID = {0,0,0,0,0,0,0,1};

	private static int heldID = -1;
	private boolean holdsItem = false;

	public Options(){
		define();
	}

	/*
	 * left, right or wheel
	 * */
	public void click(int mouseButton){

		//pegar o item
		if(mouseButton == 1){
			for(int i=0; i < button.length; i++){
				if(button[i].contains(Screen.mouse)){
					if(buttonID[i] != Value.airAir){
						if(buttonID[i] == Value.airTrashCan){ //delete item
							holdsItem = false;
							//heldID = Value.airAir;
						}else {
							heldID = buttonID[i];
							holdsItem = true;
						}
					}
				}
			}
		}


		if(holdsItem){
			for(int y = 0; y < Screen.room.block.length; y++){
				for(int x = 0; x < Screen.room.block[0].length; x++){
					if(Screen.room.block[y][x].contains(Screen.mouse)){
						if (Screen.room.block[y][x].groundID != 1 &&
							Screen.room.block[y][x].airID != 0){

							Screen.room.block[y][x].airID = heldID;
						}
					}
				}
			}
		}

	}

	private void define(){
		for(int i=0; i < button.length; i++){
			button[i] = new Rectangle((Screen.myWidth/2) - ((optionsWidth * (buttonSize+cellSpace))/2) + ((buttonSize+cellSpace)*i),
								(Screen.room.block[Screen.room.worldHeight-1][0].y + Screen.room.blockSize + awayFromRoom), buttonSize, buttonSize);
		}
		//(Screen.room.block[Screen.room.worldHeight-1][0].y + Screen.room.blockSize + awayFromRoom) >>> isso eh pra posicionar os botoes abaixo

	}

	public void draw(Graphics g){

		//desenha os botoes
		for(int i=0; i < button.length; i++){
			if(button[i].contains(Screen.mouse)){
				g.setColor(new Color (255,255,255, 150));
				g.fillRect( button[i].x, button[i].y, button[i].width, button[i].height);
			}
			//g.fillRect(button[i].x, button[i].y,  button[i].width, button[i].height);

			//if(buttonID[i] != Value.airAir)
				g.drawImage(Screen.tileset_tower[0], button[i].x + itemIn, button[i].y + itemIn,
							button[i].width - (itemIn*2), button[i].height - (itemIn*2), null);

		}

		//desenhar os 4 ultimos botoes
/*		for(int i= button.length/2; i < button.length; i++){
			if(button[i].contains(Screen.mouse)){
				g.setColor(new Color (255,255,255, 150));
				g.fillRect( button[i].x, button[i].y, button[i].width, button[i].height);
			}

			g.drawImage(Screen.tileset_trap[0], button[i].x + itemIn, button[i].y + itemIn,
						button[i].width - (itemIn*2), button[i].height - (itemIn*2), null);
		}
*/
		if(holdsItem){
			g.drawImage(Screen.tileset_tower[heldID], Screen.mouse.x - ( (button[0].width - (itemIn*2))/2 ) + itemIn, Screen.mouse.y - ( (button[0].width - (itemIn*2))/2 )  + itemIn,
						button[0].width - (itemIn*2), button[0].height - (itemIn*2), null);

			// g.drawImage(Screen.tileset_tower[heldID], Screen.mouse.x - ((button[0].width - (itemIn*2))/2) + itemIn, Screen.mouse.y  - ((button[0].width - (itemIn*2))/2) + itemIn, button[0].width, button[0].height - (itemIn*2), null);
		}
	}

}

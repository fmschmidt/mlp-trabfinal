import java.awt.*;

/*
 * Load up all different things we create.
 * Different levels we create
 * saving and loading.
 * create levels from 1 and 0.
 * Blocks on the ground.
 * Shooting bars.
 * */
public class Room {

	private int worldWidth = 12;
	public int worldHeight = 8;
	public int blockSize = 52;
	public Block[][] block;

	public Room(){
		define();
	}

	//define os blocos para se desenhar na tela
	private void define(){
		block = new Block[worldHeight][worldWidth];

		for(int y = 0; y < block.length; y++){
			for(int x = 0; x < block[0].length; x++){
				//(Screen.myWidth/2) - ((worldWidth*blockSize)/2) foi pra centralizar
				block[y][x] = new Block((Screen.myWidth/2) - ((worldWidth*blockSize)/2) + (x * blockSize), y * blockSize, blockSize, blockSize, Value.groundGrass, Value.airAir, 3);
			}
		}

	}

	public void draw (Graphics g){

		//desenhar o bloco
		for(int y = 0; y < block.length; y++){
			for(int x = 0; x < block[0].length; x++){
				block[y][x].draw(g);
			}
		}

		//desenhar a barrinha de fight
		for(int y = 0; y < block.length; y++){
			for(int x = 0; x < block[0].length; x++){
				block[y][x].fight(g);
			}
		}

	}

	public void physic() {
		for(int y = 0; y < block.length; y++){
			for(int x = 0; x < block[0].length; x++){
				block[y][x].physic();
			}
		}

	}

}

import java.awt.*;

/*
 * Diz valores XY para desenhar de um modo simples.
 * */
public class Block extends Rectangle{
	public int groundID;	//para saber qual tipo de coisa �. inimigo, bloco, etc..
	public int airID;
	public int towerID;

	private Rectangle towerSquare;
	private int towerSquareSize = 130;

	private int shotEnemy = -1;
	private boolean shoting = false;

	private int loseTime = 100, loseFrame = 0;


	//construtor
	public Block(int x, int y, int width, int height, int groundID, int airID, int towerID){
		setBounds(x, y, width, height);
		towerSquare = new Rectangle(x - (towerSquareSize/2), y - (towerSquareSize/2), width + (towerSquareSize), height + (towerSquareSize));
		this.groundID = groundID;
		this.airID = airID;
		this.towerID = towerID;
	}

	public void draw (Graphics g){
		g.drawImage(Screen.tileset_ground[groundID], x, y, width, height, null);

		//se nao for grama, � ar
		if(airID != Value.airAir){
			g.drawImage(Screen.tileset_air[airID], x, y, width, height, null);
			if(towerID == 3)
				g.drawImage(Screen.tileset_tower[0], x, y, width, height, null);
		}
	}

	//checar todos os inimigos dentro do range e atirar
	public void physic(){

		if(shotEnemy != -1 && towerSquare.intersects(Screen.enemies[shotEnemy])){
			shoting = true;
		}else {
			shoting = false;
		}

		if(!shoting){
			if(airID == 0){
				for(int i = 0 ; i < Screen.enemies.length; i++){
					if(Screen.enemies[i].inGame){
						if(towerSquare.intersects(Screen.enemies[i])){
							shoting = true;
							shotEnemy = i;
						}
					}
				}
			}
		}

		if(shoting){
			if(loseFrame >= loseTime){
				Screen.enemies[shotEnemy].loseHealth(1);

				loseFrame = 0;
			}else {
				loseFrame += 1;
			}

			//Screen.enemies[shotEnemy].loseHealth(1);

			if(Screen.enemies[shotEnemy].isDead()){
				shoting = false;
				shotEnemy = -1;
			}
		}

	}

	//gonna hold all fight stuffs
	public void fight(Graphics g){
		if(Screen.isDebug){
			if(airID == 0){
				g.drawRect(towerSquare.x, towerSquare.y, towerSquare.width, towerSquare.height);
			}
			if(shoting){
				g.setColor(new Color (255, 255, 0));
				g.drawLine(x + (width/2), y + (height/2), Screen.enemies[shotEnemy].x + (Screen.enemies[shotEnemy].width/2) , Screen.enemies[shotEnemy].y + (Screen.enemies[shotEnemy].height/2));

			}
		}
	}

}

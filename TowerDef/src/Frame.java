import javax.swing.*;
import java.awt.*;

/*
 * Essa classe estende JFrame.
 * Todos os m�todos est�o l�.
 * Ent�o podemos dar override. Ex: setSize
 * S� usamos essa classe para mudar o title ou whatever.
 * S� usamos a classe Screen para escrever..
 *
 *
 * */
public class Frame extends JFrame {
	public static String title = "Tower Defense - Felipe Schmidt";
	public static Dimension size = new Dimension(700,550);

	public Frame(){
		setTitle(title);
		setSize(size);
		setResizable(false);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		init();
	}

	//public void setSize(Dimension dim){
		//System.out.println("Hello");
	//}

	public void init(){
		setLayout(new GridLayout(1,1,0,0));

		Screen screen = new Screen(this);
		add(screen);

		setVisible(true);
	}

	public static void main(String args[]){
		Frame frame = new Frame();
	}

}

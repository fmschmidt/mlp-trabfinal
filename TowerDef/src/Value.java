
/*
 * Manipular todos os valores
 * Ex: groundGrass, "air"
 * */
public class Value {

	public static int groundGrass = 0;
	public static int groundRock = 1;

	public static int airAir = -1;
	public static int airGoal = 0;
	public static int airTrashCan = 1;

	public static int airTowerLaser = 2;

	public static int enemyAir = -1;
	public static int enemyGreeny = 0;
}

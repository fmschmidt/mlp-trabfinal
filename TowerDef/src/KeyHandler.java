import java.awt.event.*;
import java.awt.*;

/*
 * Vai dar a posicao do mouse.
 * Drag over, etc..
 * */
public class KeyHandler implements MouseMotionListener, MouseListener{

	public void mouseClicked(MouseEvent e) {

	}

	public void mousePressed(MouseEvent e) {
		Screen.options.click(e.getButton());
	}

	public void mouseReleased(MouseEvent e) {

	}

	public void mouseEntered(MouseEvent e) {

	}

	public void mouseExited(MouseEvent e) {

	}

	public void mouseDragged(MouseEvent e) {
		Screen.mouse = new Point((e.getX()) - ((Frame.size.width - Screen.myWidth)/2 ),
								 (e.getY()) - ((Frame.size.height - (Screen.myHeight)) - (Frame.size.width - Screen.myWidth)/2));
	}

	public void mouseMoved(MouseEvent e) {
		//para pegar as posicoes certas de acordo com os espacamentos
		Screen.mouse = new Point((e.getX()) - ((Frame.size.width - Screen.myWidth)/2 ),
								 (e.getY()) - ((Frame.size.height - (Screen.myHeight)) - (Frame.size.width - Screen.myWidth)/2));
	}

}

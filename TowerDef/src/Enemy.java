import java.awt.*;

public class Enemy extends Rectangle {

	public int xC, yC; 	//y corner, x corner
	public int healthEnemy  = 100;
	public int healthSpace = 3, healthHeight = 6;
	public int enemySize = 52;
	public int enemyID = Value.enemyAir;
	public boolean inGame = false;
	public boolean hasUpward = false; //have done the upward already
	public boolean hasDownward = false;
	public boolean hasLeft = false;
	public boolean hasRight = false;
	public int enemyWalk = 0;	//count for how long he walked
	public int upward = 0, downward = 1, right = 2, left = 3;
	public int direction = right;

	public Enemy(){

	}

	//surgimento do inimigo. onde ele nasce. e quais os limites dele.
	public void spawnEnemy(int enemyID){
		for(int y = 0; y < Screen.room.block.length; y++){
			if(Screen.room.block[y][0].groundID == Value.groundRock){
				setBounds(Screen.room.block[y][0].x, Screen.room.block[y][0].y, enemySize, enemySize);

				xC = 0;
				yC = y;
			}
		}
		this.enemyID = enemyID;
		this.healthEnemy = enemySize;

		inGame = true;
	}

	public void deleteEnemy(){
		inGame = false;
		direction = right;
		enemyWalk = 0;
	}

	public void looseHealth(){
		Screen.health -= 1;
	}

	public int walkFrame = 0, walkSpeed = 4;
	/*
	 * walking and stuff like that
	 * */
	public void physic(){
		if(walkFrame >= walkSpeed){
			if(direction == right){
				x += 1;
			}else if (direction == upward){
				y -= 1;
			}else if(direction == downward){
				y += 1;
			}

			enemyWalk += 1;

			//corners
			if(enemyWalk == Screen.room.blockSize){
				if(direction == right){
					xC += 1;
					hasRight = true;
				}else if (direction == upward){
					yC -= 1;
					hasUpward = true;
				}else if(direction == downward){
					yC += 1;
					hasDownward = true;
				}else if (direction == left){
					xC -= 1;
					hasLeft = true;
				}

				try{	//limites se tocar na grama, ele nao pode passar por cima
					if(!hasUpward)
						if(Screen.room.block[yC+1][xC].groundID == Value.groundRock){
							direction = downward;
						}
					if(!hasDownward)
						if(Screen.room.block[yC-1][xC].groundID == Value.groundRock){
								direction = upward;
						}
					if(!hasLeft)
						if(Screen.room.block[yC][xC+1].groundID == Value.groundRock){
							direction = right;
						}

					if(!hasRight)
						if(Screen.room.block[yC][xC-1].groundID == Value.groundRock){
							direction = left;
						}
				}catch(Exception e){}


				if(Screen.room.block[yC][xC].airID == Value.airGoal){
					deleteEnemy();
					looseHealth();
					System.out.println("Health: " + Screen.health);
				}


				hasUpward = false;
				hasDownward = false;
				hasLeft = false;
				hasRight = false;
				enemyWalk = 0;
			}

			walkFrame = 0;
		}else{
			walkFrame += 1;
		}
	}

	public void draw (Graphics g){
		if(inGame){
			g.drawImage(Screen.tileset_enemy[enemyID], x, y, width, height, null);
		}

		g.setColor(new Color(180, 50, 50));
		g.fillRect(x, y - (healthSpace + healthHeight), width, healthHeight);

		g.setColor(new Color(50, 180, 50));
		g.fillRect(x, y - (healthSpace + healthHeight), healthEnemy, healthHeight);

		g.setColor(new Color(0,0,0));
		g.drawRect(x, y - (healthSpace + healthHeight), healthEnemy -1 , healthHeight -1);

	}

	public void loseHealth(int amount) {

		healthEnemy -= amount;

		checkDeath();

	}

	private void checkDeath() {
		if(healthEnemy == 0){
			deleteEnemy();
		}
	}

	public boolean isDead(){
		if(inGame){
			return false;
		}else{
			return true;
		}
	}



}
